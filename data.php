<?php

$data = [
    [ 'nome' => 'Renato Rodrigo Gomes', 'cpf' => '326.814.811-55', 'nascimento' => '15/05/1995', 'email' => 'renatorodrigogomes-99@cartovale.com.br' ],
    [ 'nome' => 'Pedro Marcelo Souza', 'cpf' => '279.151.595-00', 'nascimento' => '16/09/1995', 'email' => 'pedromarcelosouza__pedromarcelosouza@dinamicaconsultoria.com' ],
    [ 'nome' => 'Yuri Caio Cavalcanti', 'cpf' => '301.532.243-55', 'nascimento' => '09/05/1995', 'email' => 'yuricaiocavalcanti-89@fazergourmet.com.br' ],
    [ 'nome' => 'Henry Antonio Eduardo Fernandes', 'cpf' => '831.038.697-46', 'nascimento' => '22/08/1995', 'email' => 'henryantonioeduardofernandes_@netjacarei.com.br' ],
    [ 'nome' => 'Benício Breno Ricardo Dias', 'cpf' => '242.862.324-92', 'nascimento' => '26/03/1995', 'email' => 'beniobrenoricardodias_@brf-br.com' ],
    [ 'nome' => 'Leonardo Pedro Henrique Pereira', 'cpf' => '556.910.266-31', 'nascimento' => '15/06/1995', 'email' => 'leonardopedrohenriquepereira..leonardopedrohenriquepereira@unimedsjc.com.br' ],
    [ 'nome' => 'Pedro Henrique Davi Diego Alves', 'cpf' => '878.491.908-66', 'nascimento' => '26/01/1995', 'email' => 'pedrohenriquedavidiegoalves-71@procivil.com.br' ],
    [ 'nome' => 'Vitor Lucas Matheus Oliveira', 'cpf' => '196.657.304-95', 'nascimento' => '17/10/1995', 'email' => 'vitorlucasmatheusoliveira-97@advogadosempresariais.com.br' ],
    [ 'nome' => 'Isaac Francisco Matheus Martins', 'cpf' => '856.325.487-16', 'nascimento' => '07/04/1995', 'email' => 'isaacfranciscomatheusmartins-96@hotmal.com' ],
    [ 'nome' => 'Fernando Rafael Paulo Barros', 'cpf' => '438.013.482-29', 'nascimento' => '11/10/1995', 'email' => 'fernandorafaelpaulobarros__fernandorafaelpaulobarros@mavex.com.br' ],
];

$clients = array();
$newId = 1;
foreach($data as $k => $item){
    $client = new Cliente;
    $client->id = $newId;
    $client->nome = $item['nome'];
    $client->cpf = $item['cpf'];
    $client->nascimento = $item['nascimento'];
    $client->email = $item['email'];

    array_push($clients, $client);
    $newId++;
}