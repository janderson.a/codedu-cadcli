<?php

require_once("Cliente.php");
require_once("data.php");


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="css/buttons.dataTables.min.css">
    <title>Básico</title>
</head>
<body>
    <div class="container"> 
        <div  class="modal fade" id="system">
            <div class="modal-dialog">
                <div class="modal-content">

                </div>
            </div>
        </div>   
             
        <h1>Clientes</h1>
  
        <div class="row">
            <table class="table sistemas">
                <thead>
                    <tr>
                        <th>Id</th>                   
                        <th>Nome</th>
                        <th>CPF</th>
                        <th>Nascimento</th>
                        <th>E-mail</th>                        
                        <th>Ação</th>
                    </tr>
                </thead>
                <tbody>              
                    <?php                   
                        foreach( $clients as $key => $client) {
                            ?>   
                            <tr>
                                <td><?=$client->id ?></td>
                                <td><?=$client->nome ?></td>
                                <td><?=$client->cpf ?></td>
                                <td><?=$client->nascimento ?></td>
                                <td><?=$client->email ?></td>
                                <td></td>
                            </tr>
                           
                            <?php
                        }
           
                    ?>
                </tbody>
            </table>
        </div><!-- row 2 -->  

        
    </div><!-- container -->    
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/dataTables.buttons.min.js"></script>
    <script>
        $(document).ready(function (){
            var table = $('table').DataTable({
                "columnDefs" : [{
                    'targets': -1,
                    "data" : null,
                    "defaultContent" : "<button>Detalhe</button>"
                },
                { "visible": false, "targets": [2,3,4]}
                ]
            });

            $('table tbody').on('click', 'button', function(){
                var data = table.row($(this).parents('tr')).data();
                alert(
                    "Nome: " + data[1] + "\n" +
                    "CPF: " + data[2] + "\n" +
                    "Nascimento: " + data[3] + "\n" +
                    "E-mail: " + data[4] 
                    
                );
            });

        });
    </script>
</body>
</html>